# Brief

This package is a process for interpreting a mission plan. The operator can specify a mission in TML language using a task-based approach and this process interprets such a specification, generating the appropriate orders for the rest of processes in Aerostack to carry out the mission.

# Subscribed Topics

- **behavior_event** ([droneMsgsROS/BehaviorEvent](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/BehaviorEvent.msg))  
Notification that a behavior has ended.


# Configuration Files

- **mission_specification_file.xml**  
The mission specification is written in a XML file, called mission_specification_file.xml, using the TML language.

---

# Publication

Martin Molina, Adrian Diaz-Moreno, David Palacios, Ramon A. Suarez-Fernandez, Jose Luis Sanchez-Lopez, Carlos Sampedro, Hriday Bavle and Pascual Campoy (2016): "Specifying Complex Missions for Aerial Robotics in Dynamic Environments". International Micro Air Vehicle Conference and Competition, IMAV 2016. Beijing, China.

---

# Process Development

**Maintainer:** Alberto Camporredondo ([alberto.camporredondo@gmail.com](mailto:alberto.camporredondo@gmail.com))

**Contributors:**

- Adrian Diaz ([adrian.diazm@outlook.com](mailto:adrian.diazm@outlook.com))

- Guillermo De Fermin ([gdefermin@gmail.com](mailto:gdefermin@gmail.com))
