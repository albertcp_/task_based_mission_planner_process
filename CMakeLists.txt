cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME task_based_mission_planner_process)
project(${PROJECT_NAME})

### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
add_definitions(-std=c++11)
#add_definitions(-std=c++0x)
#add_definitions(-std=c++03)
add_definitions(-g)


# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries

# Compiler-specific C++11 activation.
if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU")
    execute_process(
        COMMAND ${CMAKE_CXX_COMPILER} -dumpversion OUTPUT_VARIABLE GCC_VERSION)
    if (NOT (GCC_VERSION VERSION_GREATER 4.7 OR GCC_VERSION VERSION_EQUAL 4.7))
        message(FATAL_ERROR "${PROJECT_NAME} requires g++ 4.7 or greater.")
    endif ()
elseif ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
else ()
    message(FATAL_ERROR "Your C++ compiler does not support C++11.")
endif ()

set(TASK_BASED_MISSION_PLANNER_PROCESS_SOURCE_DIR
	src/source)

set(TASK_BASED_MISSION_PLANNER_PROCESS_INCLUDE_DIR
	src/include
	)

set(TASK_BASED_MISSION_PLANNER_PROCESS_SOURCE_FILES

	#General
        ${TASK_BASED_MISSION_PLANNER_PROCESS_SOURCE_DIR}/task_based_mission_planner_process.cpp
        ${TASK_BASED_MISSION_PLANNER_PROCESS_SOURCE_DIR}/task_based_mission_planner_process_main.cpp
	)

set(TASK_BASED_MISSION_PLANNER_PROCESS_HEADER_FILES

	#General
        ${TASK_BASED_MISSION_PLANNER_PROCESS_INCLUDE_DIR}/task_based_mission_planner_process.h
	)

find_package(catkin REQUIRED
                COMPONENTS roscpp std_srvs droneMsgsROS droneModuleROS task_based_mission_planner droneTrajectoryPlanner droneVisualMarkersLocalizer drone_process)

catkin_package(
        CATKIN_DEPENDS roscpp std_srvs droneMsgsROS droneModuleROS task_based_mission_planner drone_process
  )


include_directories(${TASK_BASED_MISSION_PLANNER_PROCESS_INCLUDE_DIR})
include_directories(${catkin_INCLUDE_DIRS})

add_library(task_based_mission_planner_process_lib ${TASK_BASED_MISSION_PLANNER_PROCESS_SOURCE_FILES} ${TASK_BASED_MISSION_PLANNER_PROCESS_HEADER_FILES})
add_dependencies(task_based_mission_planner_process_lib ${catkin_EXPORTED_TARGETS})
target_link_libraries(task_based_mission_planner_process_lib ${catkin_LIBRARIES})


add_executable(task_based_mission_planner_process_main ${TASK_BASED_MISSION_PLANNER_PROCESS_SOURCE_DIR}/task_based_mission_planner_process_main.cpp)
add_dependencies(task_based_mission_planner_process_main ${catkin_EXPORTED_TARGETS})
target_link_libraries(task_based_mission_planner_process_main task_based_mission_planner_process_lib)
target_link_libraries(task_based_mission_planner_process_main ${catkin_LIBRARIES})
