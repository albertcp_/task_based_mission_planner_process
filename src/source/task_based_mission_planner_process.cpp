#include "task_based_mission_planner_process.h"


void TaskBasedMissionPlanner::nextCommand()
{
  // TODO:
  // ParameterHandler::
  //   updateTableOfParameters(std::make_pair(PAR_CURRENT_TASK, current_task.getAbsoluteName()));

  Command cmd = mission.nextCommand();


  if(cmd.type == CmdType::activateBehavior)
  {
    std::cout << "[INFO] Executing behavior '" << cmd.behavior.getName() << "'\n";
    if(cmd.behavior.getArguments() != "" && cmd.behavior.getArguments() != "~")
      std::cout << substituteVariables(cmd.behavior.getArguments()) << "\n";
    executeBehavior(cmd.behavior);
  }

  else if(cmd.type == CmdType::inhibitBehavior)
  {
    std::cout << "[INFO] Stopping behavior '" << cmd.behavior.getName() << "'\n";
    if(cmd.behavior.getArguments() != "" && cmd.behavior.getArguments() != "~")
      std::cout << substituteVariables(cmd.behavior.getArguments()) << "\n";
    inhibitBehavior(cmd.behavior);
  }

  else if(cmd.type == CmdType::addBelief)
  {
    std::cout << "[INFO] Adding belief '" << substituteVariables(cmd.belief) << "'\n";
    addBelief(cmd.belief, cmd.belief_multivalued);
  }

  else if(cmd.type == CmdType::removeBelief)
  {
    std::cout << "[INFO] Removing belief '" << substituteVariables(cmd.belief) << "'\n";
    removeBelief(cmd.belief);
  }

  else if(cmd.type == CmdType::consultBelief)
  {
    std::cout << "[INFO] Consulting belief '" << substituteVariables(cmd.belief) << "'\n";
    auto vars = consultBelief(cmd.belief);
    for(auto v: vars)
    {
      std::cout << v.first << ": " << v.second << std::endl;
      VariableHandler::setVariable(v.first, v.second);
    }
  }

  else // if type == none
  {
    if(mission.ended())
    {
      endMission();
    }

    else // Returning from event
    {
      EndingStepType ending_step = ending_steps.back();
      ending_steps.pop_back();

      if(ending_step == EndingStepType::repeatTask)
      {
        mission.repeatCommandBeforeEvent();
      }

      else if(ending_step == EndingStepType::abortMission)
      {
        abortMission();
        return;
      }
      else if(ending_step == EndingStepType::endMission)
      {
        endMission();
        return;
      }

      mission.returnFromEvent();
    }
  }
}

TaskBasedMissionPlanner::TaskBasedMissionPlanner()
{
}

TaskBasedMissionPlanner::~TaskBasedMissionPlanner()
{
}

// ------------------------ ownX() Functions -------------------------- //
void TaskBasedMissionPlanner::ownSetUp()
{
  mission_started = false;
  waiting_for_behavior = false;

  std::string mission_path;

  //Mission's file name.
  std::string aux;
  ros::param::get("~mission_config_file", mission_path);

  ros::param::get("~droneId", aux);
  mission_path = aux+"/"+mission_path;

  drone_id = std::stoi(aux);

  ros::param::get("~stackPath", aux);
  mission_path = aux+"/configs/drone"+mission_path;

  ros::param::param<std::string>("~behavior_event_topic", behavior_event_topic, "behavior_event");
  ros::param::param<std::string>("~activate_behavior_service", activate_behavior_service, "activate_behavior");
  ros::param::param<std::string>("~cancel_behavior_service", cancel_behavior_service, "cancel_behavior");
  ros::param::param<std::string>("~add_belief_service", add_belief_service, "add_belief");
  ros::param::param<std::string>("~remove_belief_service", remove_belief_service, "remove_belief");
  ros::param::param<std::string>("~consult_belief_service", consult_belief_service, "consult_belief");
  ros::param::param<std::string>("~initiate_behaviors_service", initiate_behaviors_service, "initiate_behaviors");


  //Initialize mission attribute.
  Interpreter interpreter;
  mission = interpreter.generateMission(mission_path);
  std::string status;
  if(!interpreter.missionStatus(status))
  {
    std::cout << status;
    abortLoadingMission();
  }
  else
  {
    file_loaded = true;
    std::cout << status;
  }
}

void TaskBasedMissionPlanner::ownStart()
{
  mission_started = true;

  if(!file_loaded)
  {
    std::cout << COLOR_RED "[ERROR] Can't start mission, no mission file has been loaded" COLOR_RESET << std::endl;
    mission_started = false;
    return;
  }

  last_behavior_interrupted = false;

  behavior_event_subscriber =
    n.subscribe(behavior_event_topic, 1000, &TaskBasedMissionPlanner::behaviorEventCallback, this);

  activate_behavior_client =
    n.serviceClient<droneMsgsROS::BehaviorSrv>(activate_behavior_service);
  cancel_behavior_client =
    n.serviceClient<droneMsgsROS::BehaviorSrv>(cancel_behavior_service);
  add_belief_client =
    n.serviceClient<droneMsgsROS::AddBelief>(add_belief_service);
  remove_belief_client =
    n.serviceClient<droneMsgsROS::RemoveBelief>(remove_belief_service);
  consult_belief_client =
    n.serviceClient<droneMsgsROS::ConsultBelief>(consult_belief_service);
  initiate_behaviors_client =
    n.serviceClient<droneMsgsROS::InitiateBehaviors>(initiate_behaviors_service);

  droneMsgsROS::InitiateBehaviors srv;
  initiate_behaviors_client.call(srv);
}

void TaskBasedMissionPlanner::ownStop()
{
  if(!mission_started) return;

  mission_started = false;

  activate_behavior_client.shutdown();
  cancel_behavior_client.shutdown();
  add_belief_client.shutdown();
  remove_belief_client.shutdown();
  consult_belief_client.shutdown();
  initiate_behaviors_client.shutdown();

  ParameterHandler::reset();
  VariableHandler::reset();
  mission.reset();
}

void TaskBasedMissionPlanner::ownRun()
{
  if(mission_started)
  {
    if(!waiting_for_behavior)
      nextCommand();

    else
      checkEvents();

  }
}


void TaskBasedMissionPlanner::checkEvents()
{
  for(Event event: mission.getEvents())
  {
    for(Condition condition: event.getConditions())
    {
      if(event.getMacroTask() != mission.getCurrentRootTaskName() &&
         trueBelief(condition.getBelief()))
      {
        std::cout << "[INFO] Event activated: '" << event.getDescription() << "'\n";
        mission.setTaskTree(event.getMacroTask());
        waiting_for_behavior = false;
        last_behavior_interrupted = true;
        ending_steps.push_back(event.getEndingStep());
        return; // Don't check for any more events
      }
    }
  }
}

bool TaskBasedMissionPlanner::executeBehavior(Behavior behavior)
{
  droneMsgsROS::BehaviorSrv bhv_srv;
  droneMsgsROS::BehaviorCommand bhv;
  bhv.name = behavior.getName();
  bhv.arguments = substituteVariables(behavior.getArguments());
  bhv_srv.request.behavior = bhv;
  activate_behavior_client.call(bhv_srv);

  waiting_for_behavior = !behavior.isRecurrent();
  last_behavior = behavior.getName();

  return bhv_srv.response.ack;
}

bool TaskBasedMissionPlanner::inhibitBehavior(Behavior behavior)
{
  droneMsgsROS::BehaviorSrv bhv_srv;
  droneMsgsROS::BehaviorCommand bhv;
  bhv.name = behavior.getName();
  bhv_srv.request.behavior = bhv;
  cancel_behavior_client.call(bhv_srv);
  return bhv_srv.response.ack;
}

bool TaskBasedMissionPlanner::trueBelief(std::string belief)
{
  droneMsgsROS::ConsultBelief srv;
  srv.request.query = substituteVariables(belief);
  consult_belief_client.call(srv);
  return srv.response.success;
}

std::map<std::string, std::string> TaskBasedMissionPlanner::consultBelief(std::string belief)
{
  droneMsgsROS::ConsultBelief srv;
  srv.request.query = substituteVariables(belief);
  consult_belief_client.call(srv);
  std::map<std::string, std::string> vars;
  YAML::convert<std::map<std::string, std::string>>::decode(YAML::Load(srv.response.substitutions), vars);
  return vars;
}

bool TaskBasedMissionPlanner::addBelief(std::string belief, bool multivalued)
{
  droneMsgsROS::AddBelief srv;
  srv.request.belief_expression = substituteVariables(belief);
  srv.request.multivalued = multivalued;
  add_belief_client.call(srv);
  return srv.response.success;
}

bool TaskBasedMissionPlanner::removeBelief(std::string belief)
{
  droneMsgsROS::RemoveBelief srv;
  srv.request.belief_expression = substituteVariables(belief);
  remove_belief_client.call(srv);
  return srv.response.success;
}

std::string TaskBasedMissionPlanner::substituteVariables(std::string s)
{
  boost::regex var("\\+([\\w\\.]+)");
  auto replace_func = [](const boost::smatch& m) {return VariableHandler::getVariable(m[1]);};
  return boost::regex_replace(s, var, replace_func);
}

// ------------------------ Callbacks & services ----------------------- //

void TaskBasedMissionPlanner::behaviorEventCallback(const droneMsgsROS::BehaviorEvent &msg)
{
  if(last_behavior_interrupted)
  {
    last_behavior_interrupted = false;
  }
  else if(msg.name == last_behavior)
  {
    waiting_for_behavior = false;
    std::cout << "[INFO] Behavior '" << msg.name << "' ended with status '" <<
      msg.behavior_event_code << "'\n";
  }
}

void TaskBasedMissionPlanner::abortMission()
{
  std::cout << COLOR_GREEN "[INFO] Mission aborted" COLOR_RESET << std::endl;
  stop();
}

void TaskBasedMissionPlanner::endMission()
{
  std::cout << COLOR_GREEN "[INFO] Mission completed" COLOR_RESET << std::endl;
  stop();
}

void TaskBasedMissionPlanner::abortLoadingMission()
{
  std::cout << COLOR_RED "[ERROR] Mission file has not been loaded" COLOR_RESET << std::endl;
  file_loaded = false;
  stop();
}
